---
title: Home
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

# BIENVENIDO A MI BLOG DE GRAFICACIÓN Y ANIMACIÓN

#### Encontrarás todas las actividades y temáticas hechas en clases.