---
title: 'Ejercicios CSS'
---

#### Ejercicios Css para reforzar tus conocimientos

###### ## CSS SYNTAX
###### 
###### #### Change the color of all p elements to «red».
###### 
######  p {color: red;}
###### 
###### #### Change the color of the element with id=»para1″, to «red».
###### 
###### "#para1{ color:red;}"
###### 
###### ## CSS BACKGROUND
###### 
###### #### Set the background color for the page to «linen» and the background color for h1 to «lightblue».
###### 
###### body { background-color: linen; } h1 { background-color: lightblue; }
###### 
###### #### Set «paper.gif» as the background image of the page.
###### 
###### body { background-image: url(«paper.gif»); }
###### 
###### ## CSS BORDER
###### 
###### #### Set a «4px», «dotted» border for p.
###### 
###### p { border-style: dotted; border-width: 4px; }
###### 
###### #### Change the 3 border properties, so that they only show the border on the top side.
###### 
###### p { border-top-style: dotted; border-top-width: 4px; border-top-color: red; }
###### 
###### ## CSS MARGIN
###### 
###### #### Set the left margin of h1 to «20px».
###### 
###### h1 { background-color: lightblue; margin:20px; }
###### 
###### #### Set all margins for h1 to «25px».
###### 
###### h1 { background-color: lightblue; margin:25px; }
###### 
###### ## CSS PADDING
###### 
###### #### Set the top padding of p to «30px».
###### 
###### p { background-color: lightblue; padding:30px; }
###### 
###### #### Set all paddings for p to «50px».
###### 
###### p { background-color: lightblue; padding:50px; }