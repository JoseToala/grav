---
title: 'Estructura de Grav'
---

### Estructura de la carpeta Grav CMS
La carpeta de Grav contiene varios archivos, así que profundizaré un poco más en cada una de estas carpetas de nivel superior y explicaré para qué sirven:

#### /assets
La carpeta assets es utilizada por el nuevo sistema de administración de activos dentro de Grav para almacenar archivos .css y .js procesados.

#### /backup
La carpeta backup es la ubicación predeterminada para las copias de seguridad Grav.

#### /bin
La carpeta bin contiene la aplicación Grav CLI que se puede usar para realizar algunas tareas prácticas desde la línea de comandos.

#### /cache
La carpeta de caché se usa para almacenar archivos temporales en caché que Grav genera automáticamente para mejorar el rendimiento.
Esta carpeta no se debe utilizar para almacenar datos de usuarios, ya que se vacía de forma rutinaria de todos los datos.

#### /images
Grav viene con una biblioteca de manipulación de imágenes potente pero muy fácil de usar incorporada.
Estas imágenes se almacenan en la carpeta de imágenes para que puedan reutilizarse si se solicita nuevamente la misma imagen
con el mismo tamaño. Esta carpeta actúa como un caché de imágenes y está destinada a archivos generados automáticamente.

#### /logs
Cuando Grav detecta un error, o si tiene activado el registro o la creación de perfiles adicionales,
almacena los archivos de registro relevantes en la carpeta de registros.

#### /system
La carpeta del sistema es donde los archivos que hacen que Grav realmente funcione.
No debe editar nada en esta carpeta porque una actualización de Grav podría sobrescribir sus cambios.

#### /user
Esta es la carpeta más importante para la mayoría de los usuarios de Grav.
Esta carpeta es donde pasará su tiempo creando contenido, utilizando complementos y editando sus temas.

#### /vendor
La carpeta del proveedor contiene bibliotecas importantes en las que se basa Grav.
Esta carpeta es similar a la carpeta /system en que su contenido no debe editarse a menos
que esté absolutamente seguro de lo que está haciendo.