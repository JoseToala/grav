<? php
// Si el formulario es enviado
if (isset ($ _ POST ['submit'])) {

	// Asegúrese de que el campo de nombre no esté vacío
	if (recortar ($ _ POST ['contactname']) == '') {
		$ hasError = true;
	} else {
		$ name = trim ($ _ POST ['contactname']);
	}

	// Asegúrese de que el campo de asunto no esté vacío
	if (recortar ($ _ POST ['subject']) == '') {
		$ hasError = true;
	} else {
		$ subject = trim ($ _ POST ['subject']);
	}

	// Compruebe para asegurarse de que se envía una dirección de correo electrónico válida
	if (recortar ($ _ POST ['email']) == '') {
		$ hasError = true;
	} else if (! eregi ("^ [A-Z0-9 ._% -] + @ [A-Z0-9 ._% -] + \. [AZ] {2,4} $", recorte ($ _POST ['email']))) {
		$ hasError = true;
	} else {
		$ email = recortar ($ _ POST ['email']);
	}

	// Asegúrate de que los comentarios fueron ingresados
	if (recortar ($ _ POST ['mensaje']) == '') {
		$ hasError = true;
	} else {
		if (function_exists ('stripslashes')) {
			$ comments = stripslashes (trim ($ _ POST ['message']));
		} else {
			$ comentarios = recortar ($ _ POST ['mensaje']);
		}
	}

	// Si no hay error, envía el correo electrónico.
	if (! isset ($ hasError)) {
		$ emailTo = 'youremail@yoursite.com'; // Ponga su propia dirección de correo electrónico aquí
		$ body = "Nombre: $ nombre \ n \ n Correo electrónico: $ correo electrónico \ n \ n Tema: $ asunto \ n \ nComentarios: \ n $ comentarios";
		$ headers = 'From: My Site <'. $ emailTo. '>'. "\ r \ n". 'Responder a: ' . $ email;

		correo ($ emailTo, $ asunto, $ cuerpo, $ encabezados);
		$ emailSent = true;
	}
}
?>
